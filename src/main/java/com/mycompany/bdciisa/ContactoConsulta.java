/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.bdciisa;

import com.mycompany.bdciisa.dao.ConContactoJpaController;
import com.mycompany.bdciisa.dao.exceptions.NonexistentEntityException;
import com.mycompany.bdciisa.entity.ConContacto;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 *
 * @author jesoto
 */
@Path ("cconsulta")
public class ContactoConsulta {
    
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response listarSolicitudes(){
    
    /*    ConContacto con=new ConContacto();
        con.setRut("1-1");
        con.setNombre("Pedro");
        con.setApellidos("Espinoza");
        con.setEmail("pespinoza@gmail.com");
        con.setTelefono("987632145");
        con.setCarrera("Curso1");
        
        ConContacto con2=new ConContacto();
        con2.setRut("2-2");
        con2.setNombre("Luis");
        con2.setApellidos("Espinola");
        con2.setEmail("pespinola@gmail.com");
        con2.setTelefono("897632145");
        con2.setCarrera("Curso2");
        
        List<ConContacto> lista=new ArrayList<ConContacto>();
        lista.add(con);
        lista.add(con2);
        */
    ConContactoJpaController dao=new ConContactoJpaController();
    
    List<ConContacto> lista=dao.findConContactoEntities();
    
        return Response.ok (200).entity(lista).build();
    }
    @POST
    @Produces(MediaType.APPLICATION_JSON)
    public Response add (ConContacto concontacto){
    
        try {
            ConContactoJpaController dao=new ConContactoJpaController();
            dao.create(concontacto);
        } catch (Exception ex) {
            Logger.getLogger(ContactoConsulta.class.getName()).log(Level.SEVERE, null, ex);
        }
      
         return Response.ok (200).entity(concontacto).build();
    }
    
     @DELETE
     @Path("/{iddelete}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response delete (@PathParam ("iddelete") String iddelete){
    
        try {
            ConContactoJpaController dao=new ConContactoJpaController();
            
            dao.destroy(iddelete);
        } catch (NonexistentEntityException ex) {
            Logger.getLogger(ContactoConsulta.class.getName()).log(Level.SEVERE, null, ex);
        }
      
         return Response.ok("cliente eliminado").build();
    }
    
    @PUT
    public Response update (ConContacto concontacto){
        try {
            ConContactoJpaController dao=new ConContactoJpaController();
            
            dao.edit(concontacto);
        } catch (Exception ex) {
            Logger.getLogger(ContactoConsulta.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return Response.ok (200).entity(concontacto).build();
    }
    
}
