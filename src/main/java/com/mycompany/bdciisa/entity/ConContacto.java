/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.bdciisa.entity;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author jesoto
 */
@Entity
@Table(name = "con_contacto")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "ConContacto.findAll", query = "SELECT c FROM ConContacto c"),
    @NamedQuery(name = "ConContacto.findByRut", query = "SELECT c FROM ConContacto c WHERE c.rut = :rut"),
    @NamedQuery(name = "ConContacto.findByNombre", query = "SELECT c FROM ConContacto c WHERE c.nombre = :nombre"),
    @NamedQuery(name = "ConContacto.findByApellidos", query = "SELECT c FROM ConContacto c WHERE c.apellidos = :apellidos"),
    @NamedQuery(name = "ConContacto.findByEmail", query = "SELECT c FROM ConContacto c WHERE c.email = :email"),
    @NamedQuery(name = "ConContacto.findByTelefono", query = "SELECT c FROM ConContacto c WHERE c.telefono = :telefono"),
    @NamedQuery(name = "ConContacto.findByCarrera", query = "SELECT c FROM ConContacto c WHERE c.carrera = :carrera")})
public class ConContacto implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 2147483647)
    @Column(name = "rut")
    private String rut;
    @Size(max = 2147483647)
    @Column(name = "nombre")
    private String nombre;
    @Size(max = 2147483647)
    @Column(name = "apellidos")
    private String apellidos;
    // @Pattern(regexp="[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?", message="Invalid email")//if the field contains email address consider using this annotation to enforce field validation
    @Size(max = 2147483647)
    @Column(name = "email")
    private String email;
    @Size(max = 2147483647)
    @Column(name = "telefono")
    private String telefono;
    @Size(max = 2147483647)
    @Column(name = "carrera")
    private String carrera;

    public ConContacto() {
    }

    public ConContacto(String rut) {
        this.rut = rut;
    }

    public String getRut() {
        return rut;
    }

    public void setRut(String rut) {
        this.rut = rut;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellidos() {
        return apellidos;
    }

    public void setApellidos(String apellidos) {
        this.apellidos = apellidos;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public String getCarrera() {
        return carrera;
    }

    public void setCarrera(String carrera) {
        this.carrera = carrera;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (rut != null ? rut.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ConContacto)) {
            return false;
        }
        ConContacto other = (ConContacto) object;
        if ((this.rut == null && other.rut != null) || (this.rut != null && !this.rut.equals(other.rut))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.mycompany.bdciisa.entity.ConContacto[ rut=" + rut + " ]";
    }
    
}
